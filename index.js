let http = require("http");
const port = 4000
	let wlcm = "Welcome to Booking System."
	let profileMessage = "Welcome to your Profile!";

	let courseDir = [
		{
			"courseName": "Mathematics in the Modern World.",

		},
		{
			"courseName": "Fundamentals of Programming.",
		},

	]



	const server = http.createServer((request, response) =>
			{
				if(request.url == "/" && request.method == "GET"){
					response.writeHead(200, {'Content-Type':'application/json'});
					response.end(profileMessage);
				}

				if(request.url == "/profile" && request.method == "GET"){
					response.writeHead(200, {'Content-Type':'application/json'});
					response.end(profileMessage);
				}

				if(request.url == "/courses" && request.method == "GET"){
					response.writeHead(200, {'Content-Type':'application/json'});
					response.write("Here's our courses Available \n");
					response.write(JSON.stringify(courseDir));
					response.end();
				}

				// if(request.url == "/addcourse" && request.method == "GET"){
				// 	response.writeHead(200, {'Content-Type':'application/json'});
				// 	response.write(JSON.stringify(courseDir));
				// 	response.end();
				// }

				if(request.url == "/addcourse" && request.method == "POST"){
					
					let requestBody = "";

					request.on('data', function(data){

						requestBody += data;
					});

					request.on('end', function(){

						console.log(typeof requestBody);

						requestBody = JSON.parse(requestBody);

						let newCourse = {
							"courseName": requestBody.courseName,
						}

						courseDir.push(newCourse);
						console.log(courseDir);

						response.writeHead(200, {'Content-Type':'application/json'});
						response.write(JSON.stringify(newCourse));
						response.end("\n Added courses to our Dirctory.");
					});
				}

			}).listen(port)

console.log(`Server running at ${port}.`)